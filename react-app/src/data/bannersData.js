// bannersData.js

const bannersData = [
    {
        id: 1,
        header: "Zuitt Coding Bootcamp",
        subheader: "Opportunities for everyone, everywhere.",
        button: "Enroll now!",
        link: "/courses"
    },
    {
        id: 2,
        header: "Error 404 - Page not found.",
        subheader: "The page you are looking for cannot be found.",
        button: "Back to Home",
        link: "/"
    }
]

export default bannersData;