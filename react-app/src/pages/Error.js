// Error.js

import Banner from '../components/Banner';
import bannersData from '../data/bannersData';

export default function Error() {

	console.log(bannersData);
	const banners = bannersData
		.filter((banner) => banner.id === 2)
		.map((banner) => {
			return <Banner key={banner.id} banner={banner} />;
		});

	return (
		<>
		<>{banners}</>
		</>
	)
}