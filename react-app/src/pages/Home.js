// Home.js

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import bannersData from '../data/bannersData';

export default function Home() {

	console.log(bannersData);
	const banners = bannersData
		.filter((banner) => banner.id === 1)
		.map((banner) => {
			return <Banner key={banner.id} banner={banner} />;
		}); 

	return (
		<>
		<>{banners}</>		
		< Highlights/>
		</>
	)
}