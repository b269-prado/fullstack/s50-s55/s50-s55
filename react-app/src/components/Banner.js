// Banner.js
// Parang Jumbotron Bootstrap

// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from	'react-bootstrap/Col';

import { Button, Row, Col } from 'react-bootstrap';
import {NavLink} from 'react-router-dom';

export default function Banner({banner}) {

    const { header, subheader, button, link } = banner;

return (
    <Row>
        <Col className="p-5">
            <h1>{header}</h1>
            <p>{subheader}</p>
            <Button variant="primary" as={NavLink} to={link}>{button}</Button>
        </Col>
    </Row>
    )
}


