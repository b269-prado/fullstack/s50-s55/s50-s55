// CourseCard.js

import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';

// [S50 ACTIVITY]
import { Row, Col, Card, Button } from 'react-bootstrap';

export default function CourseCard({course}) {

	// Deconstruct the course properties into their own variables
	const { name, description, price, _id } = course;

	/*
	SYNTAX:
		const [getter, setter] = useState(initialGetterValue);
	*/

	// [S51 ACTIVITY] 
// 	const [count, setCount] = useState(0);
// 	const [seats, setSeats] = useState(30);

// 	function enroll () {
// 		// if (count === 30 || seat === 0) {
// 		// 	alert("No more seats available!");			
// 		// 	return;
// 		// };	

// 		setCount(count + 1)
// 		setSeats(seats - 1)					
// 	};
// 	// [S51 ACTIVITY END] 

// useEffect(() => {
// 	if(seats <= 0){
// 		alert("No more seats available!")		
// 	}
// }, [seats]);

	return (
		// <Card>			
		// 	<Card.Body>
		// 		<Card.Title>Sample Course</Card.Title>
		// 		<Card.Text>
		// 			<strong>Description:</strong><br />
		// 			This is a sample course offering.
		// 			<br /><br />
		// 			<strong>Price:</strong><br />
		// 			Php 40,000
		// 			</Card.Text>
		// 		<Button variant="primary">Enroll</Button>
		// 	</Card.Body>
		// </Card>
		<Row className="mt-3 mb-3">		
			<Col xs={12}>
			    <Card className="cardHighlight p-0">
			        <Card.Body>
			            <Card.Title><h4>{name}</h4></Card.Title>
			            <Card.Subtitle>Description</Card.Subtitle>
			            <Card.Text>{description}</Card.Text>
			            <Card.Subtitle>Price</Card.Subtitle>
			            <Card.Text>{price}</Card.Text>
			            {/*<Card.Subtitle>Enrollees: {count} </Card.Subtitle>
			            <Button variant="primary" onClick={enroll} disabled={ seats<=0 }>Enroll</Button>*/}
			            <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
			        </Card.Body>
			    </Card>
			</Col>
		</Row>
	)
}
// [S50 ACTIVITY END]